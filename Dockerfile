FROM node:14 as build

WORKDIR /app

COPY . .

RUN yarn install
RUN yarn build

FROM nginx as app

COPY --from=build /app/dist /usr/share/nginx/html/dist
COPY --from=build /app/fonts /usr/share/nginx/html/fonts
COPY --from=build /app/index.html /app/cv.pdf /app/favicon.ico /usr/share/nginx/html/
COPY --from=build /app/Produced-By-Human-Not-By-AI-Badge-black.png /usr/share/nginx/html/Produced-By-Human-Not-By-AI-Badge-black.png
